(* TODO:
 * - [ ] Fix usage message
 * - [ ] What is DT?
 * - [ ] What should FILE contain
 *)
let main () =
  let usage_msg =
    "touch [-chamdtr] [FILE]...\n\nConcatenate FILE(s) and print them to stdout"
  in
  let input_files = ref [] in
  let create_file = ref true in
  let follow_links = ref true in
  let change_atime = ref false in
  let change_mtime = ref false in
  let datetime_to_use = ref "" in
  let use_files_datetime = ref "" in
  let speclist =
    [ "-c", Arg.Unit (fun () -> create_file := false), "Don't create files"
    ; "-h", Arg.Unit (fun () -> follow_links := false), "Don't follow links"
    ; "-a", Arg.Unit (fun () -> change_atime := true), "Change only atime"
    ; "-m", Arg.Unit (fun () -> change_mtime := true), "Change only mtime"
    ; "-d", Arg.Set_string datetime_to_use, "Date/time to use"
    ; "-t", Arg.Set_string datetime_to_use, "Date/time to use"
    ; "-r", Arg.Set_string use_files_datetime, "Use FILE's date/time"
    ]
  in
  let anon_fun filename = input_files := filename :: !input_files in
  Arg.parse speclist anon_fun usage_msg;
  (* let now = Sys.time in *)
  let operate filename =
    if !create_file && Bool.not (Sys.file_exists filename)
    then (
      let oc = open_out filename in
      output_string oc "";
      flush oc;
      close_out oc)
    else ()
  in
  !input_files |> List.rev |> List.iter operate
;;
