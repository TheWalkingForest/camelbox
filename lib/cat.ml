let main () =
  let usage_msg = "cat [-u] [FILE]...\n\nConcatenate FILE(s) and print them to stdout" in
  let input_files = ref [] in
  let end_with_sign = ref false in
  let show_line_num = ref false in
  let show_line_num_nonempty = ref false in
  let show_nonprinting = ref false in
  let show_tabs = ref false in
  let speclist =
    [ "-n", Arg.Unit (fun () -> show_line_num := true), "Number output lines"
    ; "-b", Arg.Unit (fun () -> show_line_num_nonempty := true), "Number nonempty lines"
    ; ( "-v"
      , Arg.Unit (fun () -> show_nonprinting := true)
      , "Show nonprinting characters as ^x or M-x" )
    ; "-t", Arg.Unit (fun () -> show_tabs := true), "... and tabs as ^I"
    ; "-e", Arg.Unit (fun () -> end_with_sign := true), "... and end lines with $"
    ; ( "-A"
      , Arg.Unit
          (fun () ->
            show_nonprinting := true;
            show_tabs := true;
            end_with_sign := true)
      , "Same as -vte" )
    ]
  in
  let anon_fun filename =
    if Sys.file_exists filename
    then input_files := filename :: !input_files
    else Printf.printf "tac: %s: No such file or directory" filename
  in
  Arg.parse speclist anon_fun usage_msg;
  let n = ref 1 in
  let print_file filename =
    let print_line (line : string) =
      let l = ref line in
      if !end_with_sign then l := !l ^ "$" else ();
      if !show_line_num || !show_line_num_nonempty
      then
        if String.length line == 0 && !show_line_num_nonempty
        then ()
        else (
          l := Printf.sprintf "%6d  " !n ^ !l;
          n := !n + 1)
      else ();
      if !show_tabs
      then (
        l := Str.global_replace (Str.regexp "\t") "^I" !l;
        ())
      else ();
      (* TODO: Print non printing characters *)
      (* if !show_nonprinting *)
      (* then ( *)
      (*   l := Str.global_replace (Str.regexp {|[\x00-\x1F\x7F-\xFF]|}) "^x" !l; *)
      (*   ()) *)
      (* else (); *)
      print_endline !l
    in
    match Utils.read_to_list filename with
    | Some contents -> contents |> List.iter print_line
    | None -> ()
  in
  !input_files |> List.rev |> List.iter print_file
;;
