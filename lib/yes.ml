let main () =
  let usage_msg =
    "yes [OPTIONS] [STRING]\n\nRepeatedly output a line with STRING, or 'y'"
  in
  let string = ref "y" in
  let anon_fun str =
    if !string == "y" then string := str else string := !string ^ " " ^ str
  in
  let speclist = [] in
  Arg.parse speclist anon_fun usage_msg;
  while true do
    print_endline !string
  done
;;
