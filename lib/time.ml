(* TODO:
 * - [ ] Show timezone with %z
 *)
let parse_datestr date_str =
  let result = ref date_str in
  (* let _fmt_str = *)
  (*   [ "%R" (* /* HH:MM */ *) *)
  (*   ; "%T" (* /* HH:MM:SS */ *) *)
  (*   ; "%m.%d-%R" (* /* mm.dd-HH:MM */ *) *)
  (*   ; "%m.%d-%T" (* /* mm.dd-HH:MM:SS */ *) *)
  (*   ; "%Y.%m.%d-%R" (* /* yyyy.mm.dd-HH:MM */ *) *)
  (*   ; "%Y.%m.%d-%T" (* /* yyyy.mm.dd-HH:MM:SS */ *) *)
  (*   ; "%b %d %T %Y" (* /* month_name d HH:MM:SS YYYY */ *) *)
  (*   ; "%Y-%m-%d %R" (* /* yyyy-mm-dd HH:MM */ *) *)
  (*   ; "%Y-%m-%d %T" *)
  (*     (* /* yyyy-mm-dd HH:MM:SS */ *) *)
  (*     (* # if ENABLE_FEATURE_TIMEZONE *) *)
  (*   ; "%Y-%m-%d %R %z" (* /* yyyy-mm-dd HH:MM TZ */ *) *)
  (*   ; "%Y-%m-%d %T %z" *)
  (*     (* /* yyyy-mm-dd HH:MM:SS TZ */ *) *)
  (*     (* # endif *) *)
  (*   ; "%Y-%m-%d %H" (* /* yyyy-mm-dd HH */ *) *)
  (*   ; "%Y-%m-%d" (* /* yyyy-mm-dd */ *) *)
  (*   ] *)
  (* in *)
  let now = Unix.time () |> Unix.localtime in
  let _fmt = "" in
  let r = Str.regexp {|%R|} in
  let t = Str.regexp {|%T|} in
  let m = Str.regexp {|%m|} in
  let d = Str.regexp {|%d|} in
  let y = Str.regexp {|%Y|} in
  (* let z = Str.regexp {|%z|} in *)
  let h = Str.regexp {|%H|} in
  result := Str.global_replace r (Printf.sprintf "%d:%d" now.tm_hour now.tm_min) !result;
  result
    := Str.global_replace
         t
         (Printf.sprintf "%d:%d:%d" now.tm_hour now.tm_min now.tm_sec)
         !result;
  result := Str.global_replace m (Printf.sprintf "%d" now.tm_mon) !result;
  result := Str.global_replace d (Printf.sprintf "%d" now.tm_mday) !result;
  result := Str.global_replace y (Printf.sprintf "%d" now.tm_year) !result;
  result := Str.global_replace h (Printf.sprintf "%d" now.tm_hour) !result;
  result
;;
