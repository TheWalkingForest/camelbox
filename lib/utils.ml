let channel_to_list ic =
  let list = ref [] in
  try
    while true do
      let line = input_line ic in
      list := line :: !list
    done;
    flush stdout;
    close_in ic;
    Some (List.rev !list)
  with
  | End_of_file ->
    close_in ic;
    Some (List.rev !list)
  | _ ->
    close_in_noerr ic;
    None
;;

let channel_to_string ic =
  let content = ref "" in
  try
    while true do
      let line = input_line ic in
      content := !content ^ line
    done;
    flush stdout;
    close_in ic;
    Some !content
  with
  | End_of_file ->
    close_in ic;
    Some !content
  | _ ->
    close_in_noerr ic;
    None
;;

let read_to_list filename = channel_to_list (open_in filename)
let read_to_string filename = channel_to_string (open_in filename)

let usage () =
  print_endline "Usage: camelbox [function [arguments]...]\n";
  print_endline "   or: camelbox --list\n";
  print_endline "   or: function [arguments]...\n";
  print_endline "";
  print_endline "Currently defined functions:";
  print_endline "        cat, false, tac, true, touch, yes\n"
;;
