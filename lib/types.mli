type camelbox_error =
  | UnknownCommand of string
  | Other of string

type 'a camelbox_result =
  | Err of camelbox_error
  | Ok of 'a

module Command : sig
  type t

  val get_cmd : string -> t camelbox_result
  val main : t -> unit
end
