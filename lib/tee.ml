(* TODO:
 * - [ ] Properly handle newlines 
 *)
let main () =
  let usage_msg = "tee [-ai] [FILE]...\n\nCopy stdin to each FILE, and also to stdout" in
  let input_files = ref [] in
  let append_to_files = ref false in
  let ignore_intrupt = ref false in
  let speclist =
    [ ( "-a"
      , Arg.Unit (fun () -> append_to_files := true)
      , "Append to the given FILESs, don't overwrite" )
    ; ( "-i"
      , Arg.Unit (fun () -> ignore_intrupt := true)
      , "Ignore interrupt signals (SIGINT)" )
    ]
  in
  let anon_fun filename = input_files := filename :: !input_files in
  Arg.parse speclist anon_fun usage_msg;
  let contents =
    match Utils.channel_to_string stdin with
    | None -> ""
    | Some c -> c
  in
  let operate filename =
    let oc =
      if !append_to_files
      then open_out_gen [ Open_creat; Open_text; Open_append ] 0o640 filename
      else open_out filename
    in
    if String.length contents == 0
    then (
      output_string stdout "\n";
      output_string oc "\n";
      flush stdout;
      flush oc)
    else (
      output_string stdout contents;
      output_string oc contents;
      flush stdout;
      flush oc)
  in
  !input_files |> List.rev |> List.iter operate
;;
