let print_head contents num_lines =
  let count = ref 0 in
  while !count < num_lines && !count < List.length contents do
    print_endline (List.nth contents !count);
    count := !count + 1
  done
;;

let print_except contents trim_lines =
  let count = ref 0 in
  while !count < List.length contents - trim_lines && !count < List.length contents do
    print_endline (List.nth contents !count);
    count := !count + 1
  done
;;

(* TODO:
 * - [ ] Fix usage message
 * - [ ] Implement reading bytes
 * - [ ] Figure out what 'bkm' means
 *)
let main () =
  let usage_msg =
    "head [-nqv] [FILE]...\n\nConcatenate FILE(s) and print them to stdout"
  in
  let input_files = ref [] in
  let num_lines = ref 10 in
  let num_trim_lines = ref 0 in
  let print_headers = ref false in
  let no_print_headers = ref false in
  let speclist =
    [ ( "-n"
      , Arg.Int
          (fun n ->
            if n >= 0 then num_lines := n else num_trim_lines := Int.abs n)
      , "Print first N lines" )
    (* ; "-n", Arg.Unit (fun () -> ()), "Print first N bytes" *)
    ; "-q", Arg.Unit (fun () -> no_print_headers := true), "Never print headers"
    ; "-v", Arg.Unit (fun () -> print_headers := true), "Always print headers"
    ]
  in
  let anon_fun filename =
    if Sys.file_exists filename
    then input_files := filename :: !input_files
    else Printf.printf "head: %s: No such file or directory" filename
  in
  Arg.parse speclist anon_fun usage_msg;
  let print_file filename =
    if !print_headers && !no_print_headers == false
    then Printf.printf "==> %s <==\n" filename
    else ();
    match Utils.read_to_list filename with
    | Some contents ->
      if !num_trim_lines > 0
      then print_except contents !num_trim_lines
      else print_head contents !num_lines
    | None -> ()
  in
  !input_files |> List.rev |> List.iter print_file
;;
