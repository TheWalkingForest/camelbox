(* TODO: if a file doesnt exist, have non zero exit code even if the other files exist *)
let main () =
  let usage_msg = "tac [FILE]...\n\nConcatenate FILE(s) and print them in reverse" in
  let input_files = ref [] in
  let speclist = [] in
  let anon_fun filename =
    if Sys.file_exists filename
    then input_files := filename :: !input_files
    else Printf.printf "tac: %s: No such file or directory" filename
  in
  Arg.parse speclist anon_fun usage_msg;
  let print_file filename =
    match Utils.read_to_list filename with
    | Some contents -> contents |> List.rev |> List.iter print_endline
    | None -> ()
  in
  !input_files |> List.rev |> List.iter print_file
;;
