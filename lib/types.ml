type camelbox_error =
  | UnknownCommand of string
  | Other of string

type 'a camelbox_result =
  | Err of camelbox_error
  | Ok of 'a

module Command = struct
  type t =
    | CamelBox
    | False
    | True
    | Yes
    | Tac
    | Cat
    | Head
    | Touch
    | Tee

  let rec get_cmd cmd =
    let name =
      try
        match cmd |> String.split_on_char '/' |> List.rev |> List.hd with
        | "Camelbox" -> Some CamelBox
        | "false" -> Some False
        | "true" -> Some True
        | "yes" -> Some Yes
        | "tac" -> Some Tac
        | "cat" -> Some Cat
        | "head" -> Some Head
        | "touch" -> Some Touch
        | "tee" -> Some Tee
        | c -> raise (Failure (Printf.sprintf "Unknown command: %s" c))
      with
      | Failure e ->
        print_endline e;
        None
    in
    match name with
    | None -> Err (Other "Failed to get command name")
    | Some CamelBox -> get_cmd Sys.argv.(1)
    | Some c -> Ok c
  ;;

  let main cmd =
    match cmd with
    | CamelBox -> Utils.usage ()
    | False -> exit 1
    | True -> exit 0
    | Yes -> Yes.main ()
    | Tac -> Tac.main ()
    | Cat -> Cat.main ()
    | Head -> Head.main ()
    | Touch -> Touch.main ()
    | Tee -> Tee.main ()
  ;;
end
