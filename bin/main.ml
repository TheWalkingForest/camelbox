open Camelbox.Types

let () =
  let cmd = Command.get_cmd Sys.argv.(0) in
  match cmd with
  | Err err ->
    (match err with
     | UnknownCommand e -> Printf.printf "Unknown command: %s" e
     | Other e -> Printf.printf "Other error: %s" e)
  | Ok c -> Command.main c
;;
